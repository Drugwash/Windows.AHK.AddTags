v4.3.1.0
- reverted the 'Send' button to original sending mode Ctrl+V
- added the 'Send 2' button for alternate sending mode (raw) on protected pages
- smiley buttons now operate in a toggle mode, showing or hiding their corresponding smiley window
- key to keep smiley window open was changed from Shift to L-Alt and operates now as toggle
- added 'simple link' mode for Yahoo! smileys - just sends the image link, accepted as is on WordPress (keep 'Ctrl' pressed when selecting a smiley)
- minor internal code changes and fixes

v4.3.1.1
- fixed a small typo that prevented validation of any text when choosing image formatting

v4.3.1.2
- fixed standard WordPress emoticon code for 'surprised' (was :O instead of :o )

v4.3.1.3
- fixed handle leaks
- minor code cleanup

v4.3.1.4
- fixed 'Send 2' button not moving on main window resize
- reduced minimum height to 250px
- added some debug output for some unexplained crash on exit under Win9x (Use DbgView for checking)

v4.3.1.5
- fixed displaying status of smiley window
- fixed some x64 incompatibility
- fixed a hung condition under Linux/WINE where WinWaitActive can't work
- fixed minor aesthetic issue with displaying hints in Edit fields
- dumped tooltip in favor of window hints due to focus issues in Linux under WINE
- added ability to hide smiley windows by right-clicking them

v4.3.1.6
- fixed minor display bug in Linux/WINE (no WM_MOUSELEAVE messages from buttons, images wouldn't revert to default after hover)
- fixed minor aesthetic issue with hints

v4.3.1.7
- fixed reversed operation in smiley window hiding indicator
- fixed more issues related to hints
- fixed smiley window not hiding when hiding main window

v4.4.0.0
- internal changes: replaced independent buttons with toolbar controls for a nicer uniform look under Win9x/XP+/WINE
- internal changes: using Lib subfolder
- minor GUI adjustments
- removed button for the MonaLisa smiley set (anyone still using it?)

v4.4.0.1
- finished implementing font color changing routine

v4.4.0.2
- added missing credits for the new icons in toolbars (thank you, Mark James at famfamfam.com)
- fixed dissapearing [x] button in Edit control
- fixed wrong window width at startup
- restore focus to main Edit field after Copy/clear
- implemented automatic save/restore for window position and size

v4.4.0.4
- internal changes: added compiler directives compatible with recent Unicode AHK versions
- changed author e-mail address from AOL to Mail.com

v4.4.0.5
- disabled clearing clipboard upon show since its data may be useful in some other field
- removed redundant empty item in tray context menu (was a workaround for older Wine versions)
- added selection menu to paragraph alignment [formerly 'Center text'] to allow Left/Center/Right alignment
