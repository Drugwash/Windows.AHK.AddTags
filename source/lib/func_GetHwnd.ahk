; � Drugwash 2011-2016, v1.1

GetHwnd(gid="1")
{
Local i, hwnd, old, ofi
i := A_Gui
old := A_DetectHiddenWindows
ofi := A_FormatInteger
SetFormat, Integer, D
DetectHiddenWindows, on
gid := gid ? gid : "1"				; Set GUI 1 as default if parameter is blank
if gid is not number				; Assume named GUI (AHK 1.1+)
	hwnd := WinExist(gid)
else if gid < 1					; Invalid GUI number
	hwnd := 0
else if gid between 1 and 99		; Valid GUI number
	{
	gid+=0
	Gui, %gid%:+LastFoundExist
	WinGet, hwnd, ID
	if i
		Gui, %i%:+LastFound	; Restore default GUI if existed
	}
else hwnd := gid				; Assume parameter is HWND
DetectHiddenWindows, %old%
SetFormat, Integer, Hex
hwnd+=0
SetFormat, Integer, %ofi%
return hwnd
}
