;==VERSIONING==>
;About:	Standard color chooser dialog
;Compat:	B1.0.48.05
;Date:	2016.12.17 16:25
;Version:	1.9.4
;Author:	Drugwash
;Contact:	drugwash@aol.com
;Home:	http://my.cloudme.com/#drugwash/
;Forum:	~
;==VERSIONING==<

; proc: in/out R=ARGB, B=ABGR	g=GUI number (1-99) or hWnd or window name
;================================================================
ChgColor(res=0x00FFFFFF, proc="RR", g="1")
;================================================================
{
Static
Global PtrSz, Ptr, AW
if !(hGUI := GetHwnd(g))
	{
	msgbox, Error in %A_ThisFunc%():`nInvalid GUI count/name/hwnd: %g%.
	ErrorLevel:="1" 
	Return 0
	}
if !init
	{
	sz := 12 +6*PtrSz
	flags := 0x100 | 0x2 | 0x1	; CC_ANYCOLOR CC_FULLOPEN CC_RGBINIT
	VarSetCapacity(CustColors, 64)
	Loop, 16
		NumPut(0x00FFFFFF, CustColors, 4*(A_Index-1), "UInt")	; fix for NT-based systems
	init=1
	}
StringSplit, p, proc
ofi := A_FormatInteger
SetFormat, Integer, Hex
if !InStr(res, "0x")
	{
	res := "0x" res, res+=0, nopre := TRUE
	}
else nopre := FALSE
alpha := res & 0xFF000000
res &= 0x00FFFFFF
VarSetCapacity(CHOOSECOLOR, sz, 0)
NumPut(sz, CHOOSECOLOR, 0, "UInt")					; lStructSize
NumPut(hGUI, CHOOSECOLOR, 4, Ptr)					; hwndOwner
if p1=R
	res := SwColor(res)
NumPut(res, CHOOSECOLOR, 4+2*PtrSz, "UInt")			; rgbResult
NumPut(&CustColors, CHOOSECOLOR, 8+2*PtrSz, Ptr)	; *lpCustColors
NumPut(flags, CHOOSECOLOR, 8+3*PtrSz, "UInt")		; Flags
r := DllCall("comdlg32\ChooseColor" AW, Ptr, &CHOOSECOLOR)
if (ErrorLevel || !r) 
	{
	ErrorLevel:="1"
	Return 0
	}
res := NumGet(CHOOSECOLOR, 4+2*PtrSz, "UInt")		; rgbResult
if p2=R
	r := SwColor(res)
else r:= res+0
if alpha
	{
	alpha+=0
	r |= alpha
	}
StringUpper, r, r
if nopre
	{
	StringTrimLeft, r, r, 2
	if !alpha
		r := SubStr("00000000" r, -5)
	}
else
	StringReplace, r, r, X, x
SetFormat, Integer, %ofi%
return r
}
;================================================================
SwColor(res)
;================================================================
{
Local ofi, nopre, r
ofi := A_FormatInteger
SetFormat, Integer, Hex
if !InStr(res, "0x")
	{
	res := "0x" res
	nopre := TRUE
	}
r := ((res & 0xFF0000) >> 16) + (res & 0xFF00) + ((res & 0xFF) << 16) + (res & 0xFF000000)
if nopre
	StringTrimLeft, r, r, 2
SetFormat, Integer, %ofi%
return r
}
;================================================================
#include *i func_GetHwnd.ahk

/*
typedef struct {
    DWORD lStructSize;
    HWND hwndOwner;
    HWND hInstance;
    COLORREF rgbResult;
    COLORREF *lpCustColors;
    DWORD Flags;
    LPARAM lCustData;
    LPCCHOOKPROC lpfnHook;
    LPCTSTR lpTemplateName;
} CHOOSECOLOR, *LPCHOOSECOLOR;
*/
