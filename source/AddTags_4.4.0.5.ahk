; © Drugwash January 2012-January 2022
;================================================================
;	DIRECTIVES
;================================================================
;@Ahk2Exe-AddResource %A_ScriptDir%\res\smileys1a.bmp, 100
;@Ahk2Exe-AddResource %A_ScriptDir%\res\Ysmileys.bmp, 101
;@Ahk2Exe-AddResource %A_ScriptDir%\res\smileys2a.bmp, 102
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band0_0a.bmp, 105
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band0_1a.bmp, 106
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band0_2a.bmp, 107
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band0_3a.bmp, 108
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band1_0a.bmp, 109
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band1_1a.bmp, 110
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band1_2a.bmp, 111
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band2_0a.bmp, 112
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band2_1a.bmp, 113
;@Ahk2Exe-AddResource %A_ScriptDir%\res\band2_2a.bmp, 114

;@Ahk2Exe-AddResource *14 %A_ScriptDir%\res\addtags.ico, 207
;@Ahk2Exe-Let type = %A_IsUnicode ? "Unicode" : "ANSI"%
;@Ahk2Exe-Set Comments, Created in AutoHotkey 1.0.48.05. Compiled in %A_AhkVersion% Unicode. Built for my friends @ WordPress.com
;@Ahk2Exe-Set FileVersion, 4.4.0.5
;@Ahk2Exe-Set ProductVersion, 4.4.0.0
;@Ahk2Exe-SetProductName AddTags
;@Ahk2Exe-SetInternalName AddTags.ahk
;@Ahk2Exe-SetCompanyName Drugwash Hobby Programming
;@Ahk2Exe-SetDescription AddTags - Text formatting automation for web use
;@Ahk2Exe-SetCopyright Copyright © Drugwash`, v`, 2022
;@Ahk2Exe-SetLegalTrademarks This software is released under the terms of the GNU General Public License
;@Ahk2Exe-SetOrigFilename AddTags.exe

;@Ahk2Exe-UpdateManifest 0, Drugwash.HobbyProgramming.AddTags, 4.4.0.5

;@Ahk2Exe-SetMainIcon %A_ScriptDir%\res\addtags.ico
;================================================================
#Persistent
#NoEnv
#WinActivateForce
#SingleInstance, force
#MaxMem 10
#InstallKeybdHook
#InstallMouseHook
#KeyHistory 0
SetBatchLines, -1
SetKeyDelay, -1, -1, Play
SetControlDelay, -1
SetWinDelay, -1
ListLines, Off
SetFormat, Float, 1.1
SetFormat, Integer, D
SetTitleMatchMode, Slow
DetectHiddenWindows, On
CoordMode, Mouse, Relative
;#include lib\updates.ahk here or drop it in AHK's 'lib' folder
updates()
;================================================================
;	IDENTIFICATION
;================================================================
author = Drugwash
authormail := "drugwash" Chr(64) "mail" Chr(46) "com"
authorsite = https://drugwash.wordpress.com	; author official web site
comment = Adds some HTML formatting to plain text for web use
appname = AddTags
version = 4.4.0.5
releaseD = January 17, 2022
releaseV := A_IsUnicode ? "~ Unicode version with x64 support ~" : "~ ANSI version for all x86 Windows ~"
iconlocal = res\%appname%.ico
;================================================================
appdir=%A_ScriptDir%
inifile=%appdir%\%appname%_preferences.ini
wp_smileys=%A_ScriptDir%\res\smileys1a.bmp	; WordPress smileys
ml_smileys=%A_ScriptDir%\res\smileys2a.bmp	; MonaLisa smileys
y_smileys=%A_ScriptDir%\res\Ysmileys.bmp	; Yahoo! smileys
mail0 := "mailto:" authormail
link0 := "http://www" Chr(46) "autohotkey" Chr(46) "com"
yistr1 := "http://l.yimg.com/us.yimg.com/i/mesg/emoticons7/"	; generic Y! emoticons link
yistr2 := "http://l.yimg.com/a/i/us/msg/emoticons/"			; special Y! emoticons link
hkall=2		; number of hotkeys
fs=s15 Bold
fc=s8 Bold
if w9x
	{
	keys=#,^,!,+
	defhk=^+t,^+v
	}
else
	{
	keys=<#,>#,<^,>^,<!,>!,<+,>+
	defhk=<^+t,<^+v
	}
defHk1 := "L.CTRL+L.SHIFT+T"
defHk1x := "CTRL+SHIFT+T"
defHk2 := "L.CTRL+L.SHIFT+V"
defHk2x := "CTRL+SHIFT+V"
EnvGet, isWine, WINEPREFIX

sm := "smile :),grin :D,sad :(,uneasy :/,surprised :o,confused :?,cool :cool:,angry :x,tongue :P,neutral :|
,wink `;),lol :lol:,oops :oops:,cry :cry:,evil :evil:,twisted :twisted:,rollEyes :roll:,mrGreen :mrgreen:
,exclaim :!:,question :?:,mindblown2 o_O,evilGrin >:D,blush ^^',bear :bear:,kitten =^-^=,heart <3
,star :star:,arrow :arrow:,idea :idea:,,mindblown O_o,facepalm :facepalm:,,developer :developer:
,burrito :burrito:,martini >-I,whiterussian |_|,wordpress (w),,"

mlsm := "bye :bye:,good :good:,negative :negative:,scratch :scratch:,wacko :wacko:,yahoo :yahoo:
,yes :yes:,cry :cry:,mail :mail:,sad :-(,unsure :unsure:,wink `;-),cool B-),heart :heart:,rose :rose:
,smile :-),whistle :whistle:"

ysm := "happy :) 1,kiss :-* 11,laughing :)) 21,wave :-h 103,silly <8-} 35,waiting :-w 45,puppy-eyes :o3 108
,flag **== 55,money-eyes $-) 64,not-worthy ^:)^ 77,sad :( 2,broken-heart =(( 12,straight-face :| 22
,timeout :-t 104,party <:-P 36,sigh :-< 46,I-don't-know :-?? 106,pumpkin (~~) 56,whistling :-"" 65
,oh-go-on :-j 78,winking `;) 3,surprise :-O 13,raised-eyebrows /:) 23,daydreaming 8-> 105,yawn (:| 37
,phbbbbt >:P 47,I'm-not-listening `%-( 107,coffee ~O) 57,beat-up b-( 66,star (*) 79,big-grin :D 4
,angry X( 14,ROFL =)) 24,sleepy I-) 28,drooling =P~ 38,cowboy <):) 48,pig :@) 49,idea *-:) 58
,peace :)>- 67,hiro o-> 72,batting-eyelashes `;`;) 5,smug :> 15,angel O:-) 25,rolling-eyes 8-| 29
,thinking :-? 39,I-don't-wanna-see X_X 109,cow 3:-O 50,skull 8-X 59,shame-on-you [-X 68,billy o=> 73
,big-hug >:D< 6,cool B-) 16,nerd :-B 26,loser L-) 30,d'oh #-o 40,hurry-up :!! 110,monkey :(|) 51,bug =:) 60
,dancing \:D/ 69,april o-+ 74,confused :-/ 7,worried :-S 17,talk-to-the-hand =`; 27,sick :-& 31
,applause =D> 41,rock-on \m/ 111,chicken ~:> 52,alien >-) 61,bring-it-on >:/ 70,yin-yang (`%) 75
,love-struck :x 8,whew #:-S 18,call-me :-c 101,don't-tell-anyone :-$ 32,nail-biting :-SS 42
,thumbs-down :-q 112,rose @}`;- 53,frustrated :-L 62,hee-hee `;)) 71,bee :bz 115,blushing :""> 9
,devil >:) 19,on-the-phone :)] 100,no-talking [-( 33,hypnotised @-) 43,thumbs-up :-bd 113
,good-luck `%`%- 54,praying [-O< 63,chatterbox :-@ 76,transformer [..] transformer,tongue :P 10
,crying :(( 20,at-wits'-end ~X( 102,clown :O) 34,liar :^o 44,it-wasn't-me ^#(^ 114,pirate :ar! pirate_2"

if !A_IsCompiled
	{
	if1=res\band0_0a.bmp	; Normal
	if2=res\band0_1a.bmp	; Hot
	if3=res\band0_2a.bmp	; Disabled
	if4=res\band0_3a.bmp	; Pressed (Vista+)
	if5=res\band1_0a.bmp	; N
	if6=res\band1_1a.bmp	; H
	if7=res\band1_2a.bmp	; D
	if8=res\band2_0a.bmp	; N
	if9=res\band2_1a.bmp	; H
	if10=res\band2_2a.bmp	; D
	Loop, 10
		ir%A_Index%=0
	}
else Loop, 10
		{
		if%A_Index% := A_ScriptFullPath		; image file
		ir%A_Index% := 104+A_Index		; image resource
		}

tt := "Bold <strong>|Italic <em>|Underline (span style)|Strikeout <del>|Add URL|Add picture
|Quote (English comment)|Code|Indent|Center text|Unordered list|Select text color|Font size|Inserted text
|Abbreviation|Acronym|Convert to UPPERCASE|Convert to lowercase|More|Heading type|Horizontal ruler
|WordPress Smileys|Yahoo! smileys|Superscript|Footnote|Mona Lisa smileys|Apply text color"

tta := "Bold <b>|Italic <i>|Underline <u>|Strikeout <strike>|Add URL (nofollow)|Add picture with border
|Quote (Romanian  comment)|Code|Indent|Align text|Ordered list|Select text color|Font size|Inserted text
|Abbreviation|Acronym|Convert to UPPERCASE|Convert to lowercase|More|Heading type|Horizontal ruler
|WordPress Smileys|Yahoo! smileys|Superscript|Footnote|Mona Lisa smileys|Apply text color"
StringSplit, tt, tt, |
StringSplit, tta, tta, |
gosub init
k1 := ahsm ? "0" : "1"	; keep smiley window open toggle switch
;_____________________________________________________________________________________
Menu, Tray, UseErrorLevel
if !A_IsCompiled
	Menu, Tray, Icon, %iconlocal%
Menu, Tray, Tip, %appname% %version%
Menu, Tray, NoStandard
Menu, Tray, Add, Disable hotkey, hkoff
Menu, Tray, Add, Show window, action1
Menu, Tray, Default, Show window
Menu, Tray, Add, AlwaysOnTop, aot
if aot
	Menu, Tray, Check, AlwaysOnTop
Menu, Tray, Add, Settings, settings
Menu, Tray, Add
Menu, Tray, Add, About, about
Menu, Tray, Add, Help, readme
Menu, Tray, Add
Menu, Tray, Add, Reload, reload
Menu, Tray, Add, Exit, exit
;Menu, Tray, Add, , exit		; this was a workaround for menu showing beneath taskbar/panel in Linux

Menu, txtsize, Add, XX-Small, size1
Menu, txtsize, Add, Small, size1
Menu, txtsize, Add, Medium, size1
Menu, txtsize, Add, Large, size1
Menu, txtsize, Add, X-Large, size1
Menu, txtsize, Add, XX-Large, size1
Menu, txtsize, Add, 300`%, size1

Menu, hdsize, Add, Heading 1, size2
Menu, hdsize, Add, Heading 2, size2
Menu, hdsize, Add, Heading 3, size2
Menu, hdsize, Add, Heading 4, size2
Menu, hdsize, Add, Heading 5, size2
Menu, hdsize, Add, Heading 6, size2

Menu, aligntext, Add, Left, align
Menu, aligntext, Add, Center, align
Menu, aligntext, Add, Right, align
;_____________________________________________________________________________________
Gui, % "1:+LastFound " (aot ? "+" : "-") "AlwaysOnTop +Resize +MinSize410x250 +OwnDialogs"
Gui, 1:Margin, 4, 4
Gui, 1:Font, s8, Tahoma
Gui, 1:Add, Edit, x172 y9 w232 h13 -E0x200 ReadOnly Disabled vsmout,
Gui, 1:Add, Edit, xp y+0 wp hp -E0x200 ReadOnly Disabled vsmwin, 
Loop, 10
	hTBIL%A_Index% := ILC_List(A_Index < 5 ? "32" : "16", if%A_Index%, ir%A_Index%, "N")
IL_ReplaceColor(hTBIL8, 5, SwColor("0x" color), "BW")
IL_ReplaceColor(hTBIL9, 5, SwColor("0x" color), "BW")

TC0 := "1|BBCode|0x16|,2|XHTML|0x16|,3|HTML|0x16|,4|WordPress|0x16|0x5"
TC1 := "1|Bold,2|Italic,3|Underline,4|Strikethrough,10|Convert to Uppercase,5|Link
	,6|Quote,7|Code,8|Abbreviation,9|Acronym,11|Convert to Lowercase,12|WP Smiley"
TC2 := "1|Picture,2|Indent,3|Paragraph Alignment,4|Choose Font Color,5|Apply Font Color,6|Font Size
	,7|Superscript,16|Table|0x16|0,8|YM Smiley,9|Inserted,10|List,11|More,12|Horizontal Ruler
	,13|Heading,14|Subscript|0x16|0,15|Foot Note|0x16|0"

hTB0 := TB_Create("1", "9|10|160|24", 0x8BCE, 0x89)
hTB1 := TB_Create("1", "4|42|146|46", 0x808BCE, 0x89)
hTB2 := TB_Create("1", "212|42|194|46", 0x808BCE, 0x89)
Gui, 1:Font, s7, Tahoma
Gui, 1:Add, GroupBox, x4 y0 w402 h38 0x8000 -Theme hwndhGr,
Gui, 1:Font, s8, Tahoma
hFont := GetSetFont(hGr, hTB0)
TB_SetIL(hTB0, "I0|" hTBIL1 " H" hTBIL2 " D" hTBIL3 " P0|" hTBIL4, "5")
TB_SetIL(hTB1, "I0|" hTBIL5 " H" hTBIL6 " D" hTBIL7)
TB_SetIL(hTB2, "I0|" hTBIL8 " H" hTBIL9 " D" hTBIL10)
Loop, Parse, TC0, CSV
	{
	i3 := 0x10, i4 := ""
	StringSplit, i, A_LoopField, |
	i4 := (i4 != "") ? i4 : 0x4
	TB_AddBtn(hTB0, i2, A_Index, i4, i3, i1-1, 0)
	}
Loop, Parse, TC1, CSV
	{
	i3 := 0x10, i4 := 0x4
	StringSplit, i, A_LoopField, |
	i4 := (i4 != "") ? i4 : 0x4
	TB_AddBtn(hTB1, "", A_Index, i4, i3, i1-1, 0)
	}
Loop, Parse, TC2, CSV
	{
	i3 := 0x10, i4 := 0x4
	StringSplit, i, A_LoopField, |
	i4 := (i4 != "") ? i4 : 0x4
	TB_AddBtn(hTB2, "", A_Index, i4, i3, i1-1, 0)
	}
TB_Size(hTB0), TB_Size(hTB1), TB_Size(hTB2)
i1 := i2 := i3 := i4 := ""
TB_Set(hTB0, "s0x9BCE b0x100010 d0x10001")
TB_Set(hTB1, "s0x9BCE b0x100010 d0x10001"), TB_Set(hTB2, "s0x9BCE b0x100010 d0x10001")
Loop, Parse, TC0, CSV
	{
	StringSplit, i, A_LoopField, |
	TB_BtnSet(hTB0, A_Index, "t" i2)
	}
Loop, Parse, TC1, CSV
	{
	StringSplit, i, A_LoopField, |
	TB_BtnSet(hTB1, A_Index, "t" i2)
	}
Loop, Parse, TC2, CSV
	{
	StringSplit, i, A_LoopField, |
	TB_BtnSet(hTB2, A_Index, "t" i2)
	}
i1 := i2 := i3 := i4 := ""
;== END TOOLBARS SECTION ==============
Gui, 1:Add, GroupBox, x4 y86 w402 h330 0x8000 -Theme vgb3, 
Gui, 1:Add, Button, x8 yp+10 w20 h20 vCurl gclr, x
Gui, 1:Add, Edit, x+0 yp w374 h20 +E0x10 hwndhU1 vurl gchkhint, 
Gui, 1:Font, s7, 
Gui, 1:Add, Text, xp+6 yp+1 w366 h18 0x200 BackgroundTrans Disabled hwndhE1 vhint1
	, enter Link/Image URL or Quoted nickname here
Gui, 1:Font, s8, 
Gui, 1:Add, Button, x8 y+4 w20 h20 vCurltxt gclr, x
Gui, 1:Add, Edit, x+0 yp w374 h20 +E0x10 hwndhU2 vurltxt gchkhint, 
Gui, 1:Add, ComboBox, xp yp w126 h100 R10 Hidden vnotelist, 
Gui, 1:Font, s7, 
Gui, 1:Add, Text, xp+6 yp+1 w366 h18 0x200 BackgroundTrans Disabled hwndhE2 vhint2
	, enter Alternate text, Quoted comment no. or Abbreviation/Acronym full text here
Gui, 1:Font, s8, 
Gui, 1:Add, Edit, x8 y+2 w394 h273 +0x2000000 +0x100 +E0x10 hwndhEdit vformatted geditchg, 
Gui, 1:Add, Button, xp yp w20 h20 E0x8 vbClear gclear, x
;Gui, 1:Add, Button, xp y+1 wp hp gdelete, d
;Gui, 1:Add, Button, xp y+1 wp hp vbUndo gundo, u
;Gui, 1:Add, Button, xp y+1 wp hp gredo, r
Gui, 1:Add, Button, xp y426 w60 h40 Default vbtn1 gsend, Send
Gui, 1:Add, Button, x+5 yp w45 hp vbtn0 gsendA, Send 2
Gui, 1:Add, Button, x+10 yp w40 hp vbtn2 gcopy, Copy
Gui, 1:Add, Button, x+10 yp w40 hp vbtn3 ghide, Hide
Gui, 1:Add, Button, x+10 yp w60 h20 vbtn4 gsettings, Settings
Gui, 1:Add, Button, xp y+0 w60 hp vbtn5 gabout, About
Gui, 1:Add, Button, x352 y426 w50 h40 vbtn6 gexit, Exit
edBtns=1		; edit buttons
hMain := WinExist()
GuiControl, 1:, smwin, % k1 ? "Keep smiley window open      [Alt to toggle]" : ""
;_____________________________________________________________________________________
Gui, 2:+Owner1 +AlwaysOnTop
Gui, 2:Margin, 10, 10
ydiff := w9x ? "11" : "2", es := "Center -Multi -VScroll -HScroll Limit3 Uppercase"
if w9x
	{
	Gui, 2:Add, CheckBox, xm+6 ym+25 w55 h18 Section, Win
	Gui, 2:Add, CheckBox, xp yp wp hp Hidden, R-Win
	Gui, 2:Add, CheckBox, x+5 ys wp hp, Ctrl
	Gui, 2:Add, CheckBox, xp yp wp hp Hidden, R-Ctrl
	Gui, 2:Add, CheckBox, x+5 ys wp hp, Alt
	Gui, 2:Add, CheckBox, xp yp wp hp Hidden, R-Alt
	Gui, 2:Add, CheckBox, x+5 ys wp hp, Shift
	Gui, 2:Add, CheckBox, xp yp wp hp Hidden, R-Shift
	Gui, 2:Font, %fs%, Default
	Gui, 2:Add, Edit, x+5 ys-%ydiff% w48 h38 Section %es% vk1 hwndhh1, 
	Gui, 2:Font, Norm s7, Tahoma
	Gui, 2:Add, CheckBox, xm+6 y+32 w55 h18 Section, Win
	Gui, 2:Add, CheckBox, xp yp wp hp Hidden, R-Win
	Gui, 2:Add, CheckBox, x+5 ys wp hp, Ctrl
	Gui, 2:Add, CheckBox, xp yp wp hp Hidden, R-Ctrl
	Gui, 2:Add, CheckBox, x+5 ys wp hp, Alt
	Gui, 2:Add, CheckBox, xp yp wp hp Hidden, R-Alt
	Gui, 2:Add, CheckBox, x+5 ys wp hp, Shift
	Gui, 2:Add, CheckBox, xp yp wp hp Hidden, R-Shift
	Gui, 2:Font, %fs%, Default
	Gui, 2:Add, Edit, x+5 ys-%ydiff% w48 h38 Section %es% vk2 hwndhh2, 
	Gui, 2:Font, Norm s7, Tahoma
	}
else
	{
	Gui, 2:Add, CheckBox, xm+6 ym+16 w55 h18 Section, L-Win
	Gui, 2:Add, CheckBox, xp y+2 wp hp, R-Win
	Gui, 2:Add, CheckBox, x+5 ys wp hp, L-Ctrl
	Gui, 2:Add, CheckBox, xp y+2 wp hp, R-Ctrl
	Gui, 2:Add, CheckBox, x+5 ys wp hp, L-Alt
	Gui, 2:Add, CheckBox, xp y+2 wp hp, R-Alt
	Gui, 2:Add, CheckBox, x+5 ys wp hp, L-Shift
	Gui, 2:Add, CheckBox, xp y+2 wp hp, R-Shift
	Gui, 2:Font, %fs%, Default
	Gui, 2:Add, Edit, x+5 ys-%ydiff% w48 h38 Section %es% vk1 hwndhh1, 
	Gui, 2:Font, Norm s7, Tahoma
	Gui, 2:Add, CheckBox, xm+6 y+24 w55 h18 Section, L-Win
	Gui, 2:Add, CheckBox, xp y+2 wp hp, R-Win
	Gui, 2:Add, CheckBox, x+5 ys wp hp, L-Ctrl
	Gui, 2:Add, CheckBox, xp y+2 wp hp, R-Ctrl
	Gui, 2:Add, CheckBox, x+5 ys wp hp, L-Alt
	Gui, 2:Add, CheckBox, xp y+2 wp hp, R-Alt
	Gui, 2:Add, CheckBox, x+5 ys wp hp, L-Shift
	Gui, 2:Add, CheckBox, xp y+2 wp hp, R-Shift
	Gui, 2:Font, %fs%, Default
	Gui, 2:Add, Edit, x+5 ys-%ydiff% w48 h38 Section %es% vk2 hwndhh2, 
	Gui, 2:Font, Norm s7, Tahoma
	}
Gui, 2:Add, Checkbox, xm+6 y+10 w115 h16 Checked%aot% vaot gaot, Always On Top
Gui, 2:Add, Checkbox, x+5 yp w55 hp Checked%arun% varun garun, Autorun
Gui, 2:Add, Checkbox, x+5 yp w60 hp Checked%ahide% vahide gahide, Autohide
Gui, 2:Font, s10, Wingdings
Gui, 2:Add, Checkbox, x+0 yp w40 hp Checked%ahsm% vahsm gahsm, % ahsm ? "L" : "J"
Gui, 2:Font, %fc%, Tahoma
Gui, 2:Add, GroupBox, xm ym w300 h58+%ydiff% +Center -Theme +0x8000, 
Gui, 2:Add, CheckBox, xm+6 yp-2 h18 Checked cBlue vt1 gth1, Hotkey to Show/Hide
Gui, 2:Add, GroupBox, xm ym+60+%ydiff% w300 h58+%ydiff% +Center -Theme +0x8000, 
Gui, 2:Add, CheckBox, xm+6 yp-2 h18 Checked cBlue vt2 gth2, Hotkey to Paste Special
Gui, 2:Font, Norm s8, Tahoma
Gui, 2:Add, Button, xm w60 gdefault, Default
Gui, 2:Add, Button, x+20 wp yp gsettingsOK, OK
Gui, 2:Add, Button, x+10 wp yp Hidden gsettingsHide, Cancel
Gui, 2:Add, Button, x+20 wp yp gabout, About
Gui, 2:Show, w320 Hide, %appname% settings
;_____________________________________________________________________________________
Gui, 3:Margin, 0, 0
Gui, 3:+AlwaysOnTop -Caption +ToolWindow +Border +Owner1
Gui, 3:Color, White, White
Gui, 3:Add, Picture, x0 y0 w249 h115 0xE hwndhSmile vsmile gsmile,
Gui, 3:Font, s12 Bold, Tahoma
Gui, 3:Add, Text, x2 y94 w245 h20 0x200 Center BackgroundTrans vsmCode,
Gui, 3:Show, Hide, Smileys1		; WordPress smileys
WinGet, hSW1, ID, Smileys1
;_____________________________________________________________________________________
Gui, 4:Margin, 0, 0
Gui, 4:+AlwaysOnTop -Caption +ToolWindow +Border +Owner1
Gui, 4:Color, White, White
Gui, 4:Add, Picture, x0 y0 w400 h200 0xE hwndhYSmile vysmile gysmile,
Gui, 4:Font, s9 Bold, Tahoma
Gui, 4:Add, Text, x280 y182 w120 h18 0x200 Center BackgroundTrans vysmCode,
Gui, 4:Show, Hide, Smileys2		; Yahoo! smileys
WinGet, hSW2, ID, Smileys2
;_____________________________________________________________________________________
Gui, 5:Margin, 0, 0
Gui, 5:+AlwaysOnTop -Caption +ToolWindow +Border +Owner1
Gui, 5:Color, White, White
Gui, 5:Add, Picture, x0 y0 w192 h128 0xE hwndhMLSmile vmlsmile gmlsmile,
Gui, 5:Font, s12 Bold, Tahoma
Gui, 5:Add, Text, x2 y98 w188 h28 0x200 Center BackgroundTrans vmlsmCode,
Gui, 5:Show, Hide, Smileys3		; MonaLisa smileys
WinGet, hSW3, ID, Smileys3
;_____________________________________________________________________________________
if A_IsCompiled
	{
	hBmp1 := LoadImg(0, "100")
	hBmp2 := LoadImg(0, "101")
	hBmp3 := LoadImg(0, "102")
	}
else
	{
	hBmp1 := LoadImg(wp_smileys)
	hBmp2 := LoadImg(y_smileys)
	hBmp3 := LoadImg(ml_smileys)
	}
SetBmp(hSmile, hBmp1), SetBmp(hYSmile, hBmp2), SetBmp(hMLSmile, hBmp3)
Loop, Parse, sm, CSV
	{
	i1 := i2 := ""
	StringSplit, i, A_LoopField, %A_Space%
	smT%A_Index% := i1
	smC%A_Index% := i2
	}
Loop, Parse, mlsm, CSV
	{
	i1 := i2 := ""
	StringSplit, i, A_LoopField, %A_Space%
	mlsmT%A_Index% := i1
	mlsmC%A_Index% := i2
	}
Loop, Parse, ysm, CSV
	{
	i1 := i2 := i3 := ""
	StringSplit, i, A_LoopField, %A_Space%
	StringReplace, i1, i1, -, %A_Space%, All
	ysmT%A_Index% := i1
	ysmC%A_Index% := i2
	ysmI%A_Index% := i3
	}
EditSetRect(hh1, "T6")
Loop, %tt0%
	b%A_Index%=0
FileCreateDir, %A_Temp%\%appname%
i := "sm,ysm,mlsm,io,tio,tt,tta,iconlib,ml_smileys,wp_smileys,y_smileys"
Loop, Parse, i, CSV
	VarSetCapacity(%A_LoopField%, 0)
OnMessage(0x111, "comm")	; WM_COMMAND
OnMessage(0x200, "btn1")	; WM_MOUSEMOVE
OnMessage(0x2A3, "btn2")	; WM_MOUSELEAVE
OnMessage(0x202, "btn2")	; WM_LBUTTONUP
OnMessage(0x100, "key")	; WM_KEYDOWN (apparently same with WM_KEYFIRST)
OnMessage(0x101, "keyup")	; WM_KEYUP
OnMessage(0x104, "key")	; WM_SYSKEYDOWN
OnMessage(0x105, "keyup")	; WM_SYSKEYUP

Loop, Parse, defhk, CSV
	hk%A_Index% := A_LoopField
gosub initialize
Loop, %hkall%
	Hotkey, % hk%A_Index%, action%A_Index%
IfExist, %A_ScriptDir%\%appname% readme.txt
	FileDelete, %A_ScriptDir%\%appname% readme.txt
if !ahide
	goto action1
return
;================================================================
;		END OF AUTOEXEC SECTION
;================================================================
reload:
gosub cleanup
Reload
Sleep, 1000

exit:
GuiClose:
gosub cleanup
ExitApp

cleanup:
WinGetPos, x, y,,, ahk_id %hMain%
WinPos = x%x% y%y%
if modif
	gosub save
Gui, Hide
OnMessage(0x111, "")	; WM_COMMAND
OnMessage(0x200, "")	; WM_MOUSEMOVE
OnMessage(0x2A3, "")	; WM_MOUSELEAVE
OnMessage(0x202, "")	; WM_LBUTTONUP
TB_Cleanup(hTB0), TB_Cleanup(hTB1), TB_Cleanup(hTB2)
DllCall("DeleteObject", Ptr, hFont)
Loop, 10
	IL_Destroy(hTBIL%A_Index%)
SetBmp(hSmile, 0), SetBmp(hYSmile, 0), SetBmp(hMLSmile, 0)
IfExist, %A_Temp%\%appname%
	FileRemoveDir, %A_Temp%\%appname%, 1
Sleep, 500
return
;================================================================
GuiSize:
;================================================================
GuiControl, Move, url, % "w" A_GuiWidth-36
GuiControl, Move, urltxt, % "w" A_GuiWidth-36
GuiControl, Move, formatted, % "w" A_GuiWidth-16 " h" A_GuiHeight-199
GuiControl, Move, gb3, % "w" A_GuiWidth-8 " h" A_GuiHeight-140
GuiControl, Move, btn1, % "y" A_GuiHeight-45
GuiControl, Move, btn0, % "y" A_GuiHeight-45
GuiControl, Move, btn2, % "y" A_GuiHeight-45
GuiControl, Move, btn3, % "y" A_GuiHeight-45
GuiControl, Move, btn4, % "y" A_GuiHeight-45
GuiControl, Move, btn6, % "y" A_GuiHeight-45
GuiControl, MoveDraw, btn5, % "y" A_GuiHeight-25
EditSetRect(hEdit, "L21")
WinGetPos,,, w, h, ahk_id %hEdit%
er := "0-" edBtns*21 " 21-" edBtns*21 " 21-0 " w "-0 " w "-" h " 0-" h
WinSet, Region, %er%, ahk_id %hEdit%
GuiControl, MoveDraw, bClear
WinGetPos, x, y,,, ahk_id %hMain%
WinPos = x%x% y%y%
LastW := A_GuiWidth, LastH := A_GuiHeight
modif=1
return
;================================================================
;GuiDropFiles:
;msgbox, A_GuiControl=%A_GuiControl% A_EventInfo=%A_EventInfo% A_GuiEvent=%A_GuiEvent%
return
;================================================================
init:
;================================================================
IniRead, bord, %inifile%, Preferences, PictureBorder, 0
IniRead, aot, %inifile%, Preferences, AlwaysOnTop, 1
IniRead, arun, %inifile%, Preferences, Autorun, 0
IniRead, ahide, %inifile%, Preferences, Autohide, 1
IniRead, ahsm, %inifile%, Preferences, AutohideSmileys, 1
IniRead, color, %inifile%, Preferences, LastColor, 0
IniRead, WinPos, %inifile%, Preferences, Position, Center
IniRead, LastW, %inifile%, Preferences, LastW, 410
IniRead, LastH, %inifile%, Preferences, LastH, 320
return
;================================================================
save:
;================================================================
Gui, Submit, NoHide
IniWrite, %aot%, %inifile%, Preferences, AlwaysOnTop
IniWrite, %arun%, %inifile%, Preferences, Autorun
IniWrite, %ahide%, %inifile%, Preferences, Autohide
IniWrite, %ahsm%, %inifile%, Preferences, AutohideSmileys
IniWrite, %color%, %inifile%, Preferences, LastColor
IniWrite, %WinPos%, %inifile%, Preferences, Position
IniWrite, %LastW%, %inifile%, Preferences, LastW
IniWrite, %LastH%, %inifile%, Preferences, LastH
modif=
return
;================================================================
mail0:
link0:
link1:
link2:
link3:
link4:
link5:
;================================================================
link := %A_ThisLabel%
Run, %link%,, UseErrorLevel
return
;================================================================
TB0C1:	; BBCode
TB0C2:	; XHTML
TB0C3:	; HTML
TB0C4:	; WordPress
TB_BtnSet(hTB0, SubStr(A_ThisLabel, 0), "s4")
TB_BtnSet(hTB0, 4, "s5")
MsgBox, 0x42040, %appname% information, Currently only WordPress-specific HTML codes are available.
return
;================================================================
chkhint:
;================================================================
Gui, 1:Submit, NoHide
GuiControl, 1:Hide, hint1
GuiControl, 1:Hide, hint2
GuiControl, % "1:" (url ? "Hide" : "Show"), hint1
GuiControl, % "1:" (urltxt ? "Hide" : "Show"), hint2
return
;================================================================
TB1C12:		; WP smileys window
r := DllCall("IsWindowVisible", Ptr, hSW1) ? "Hide" : "Show"
Gui, 3:%r%
goto cmnhint
;================================================================
TB2C9:		; YM smiley window
r := DllCall("IsWindowVisible", Ptr, hSW1) ? "Hide" : "Show"
Gui, 4:%r%
goto cmnhint
;================================================================
; maybe someday there will be MonaLisa set again
r := DllCall("IsWindowVisible", Ptr, hSW1) ? "Hide" : "Show"
Gui, 5:%r%
cmnhint:
GuiControl, 1:, smwin,% (r="Show" && k1) ? "Keep smiley window open      [Alt to toggle]" : ""
return
;================================================================
ysmile:		; YAHOO SMILEY SELECTION
;================================================================
MouseGetPos, x, y
optionK := GetKeyState("Ctrl", "P") | GetKeyState("LCtrl", "P") | GetKeyState("RCtrl", "P")
optionL := GetKeyState("Shift", "P") | GetKeyState("LShift", "P") | GetKeyState("RShift", "P")
i := 1 + (x-1)//40 + 10*((y-1)//20)
if !ysmC%i%
	{
	PostMessage, 0xA1,2,,, A
	return
	}
if optionK
	sts := A_Space ysmC%i%
else if optionL
	{
	if ysmT%i% in transformer,pirate
		yistr := yistr2
	else yistr := yistr1
	sts := "`r`n" yistr ysmI%i% ".gif"
	}
else
	{
	if ysmT%i% in transformer,pirate
		yistr := yistr2
	else yistr := yistr1
	sts := "<img class=""image"" src=""" yistr ysmI%i% ".gif"" alt=""" ysmT%i% """ title=""" ysmT%i% """ />"
	}
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xC2		; EM_REPLACESEL
	, "UInt"	, 0
	, "Str"	, sts)
if (!ahsm OR k1)
	return
;================================================================
4GuiEscape:
Gui, 4:Hide
GuiControl, 1:, smout, 
DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
smile:		; WORDPRESS SMILEY SELECTION
;================================================================
MouseGetPos, x, y
i := 1 + (x-1)//25 + 10*((y-1)//23)
if !smC%i%
	{
	PostMessage, 0xA1,2,,, A
	return
	}
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xC2		; EM_REPLACESEL
	, "UInt"	, 0
	, "Str"	, A_Space smC%i% A_Space)
if (!ahsm OR k1)
	return
;================================================================
3GuiEscape:
Gui, 3:Hide
GuiControl, 1:, smout, 
DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
mlsmile:		; MONALISA SMILEY SELECTION
;================================================================
MouseGetPos, x, y
i := 1 + (x-1)//32 + 6*((y-1)//32)
if !mlsmC%i%
	{
	PostMessage, 0xA1,2,,, A
	return
	}
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xC2		; EM_REPLACESEL
	, "UInt"	, 0
	, "Str"	, A_Space mlsmC%i% A_Space)
if (!ahsm OR k1)
	return
;================================================================
5GuiEscape:
Gui, 5:Hide
GuiControl, 1:, smout, 
DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
action1:
;================================================================
if !isWine
	{
	Clipboard=
	Sleep, 10
	Send ^c
	WinGetActiveTitle, awin
	Sleep, 200
	if !cliptext
		{
		Clipboard=	; for now we don't do anything,
		}			; later on we may convert clipboard contents to image
	}
info = %Clipboard%	; was 'Clipboard', we'll see how it behaves
Gui, 1:Show, %WinPos% w%LastW% h%LastH% Hide
Sleep, 10
Gui, 1:Show, %WinPos% w%LastW% h%LastH%, %appname% %version%
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xB1		; EM_SETSEL
	, "UInt"	, 0
	, "UInt"	, -1)
GuiControl, 1:, formatted, %info%
Gui, 1:Submit, NoHide
GuiControl, 1:MoveDraw, bClear
; Clipboard=		; this can sometimes be annoying
Menu, Tray, Rename, Show window, Hide window
Menu, Tray, Add, Hide window, hide
Hotkey, %hk1%, hide
WinPos := ""
DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
action2:
;================================================================
OnClipboardChange:
cliptext := A_EventInfo<2 ? TRUE : FALSE	; we use this switch so later on we may enhance the application
									; with a picture copy/paste, if needed
return
;================================================================
strg := Clipboard	; was 'Clipboard', we'll see how it behaves
SendPlay, {Raw}%strg%
return
;================================================================
editChg:
GuiControl, 1:Enable, bUndo
return
;================================================================
clear:
;================================================================
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xB1		; EM_SETSEL
	, "UInt"	, 0
	, "UInt"	, -1)
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xC2		; EM_REPLACESEL
	, "UInt"	, 0
	, "Str"	, "")
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xB9		; EM_SETMODIFY
	, "UInt"	, 1
	, "UInt"	, 0)
;GuiControl, 1:, formatted,
;Gui, 1:Submit, NoHide
GuiControl, 1:MoveDraw, formatted,
GuiControl, 1:Enable, bUndo
DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
delete:
;================================================================
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xC2		; EM_REPLACESEL
	, "UInt"	, 0
	, "Str"	, "")
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xB9		; EM_SETMODIFY
	, "UInt"	, 1
	, "UInt"	, 0)
GuiControl, 1:Enable, bUndo
DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
copy:
;================================================================
;Clipboard := formatted
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xB1		; EM_SETSEL
	, "UInt"	, 0
	, "UInt"	, -1)
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0x301		; WM_COPY
	, "UInt"	, 0
	, "UInt"	, 0)
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xB1		; EM_SETSEL
	, "UInt"	, 0
	, "UInt"	, 0)
DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
undo:
;================================================================
if DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xC6		; 0xC7 EM_UNDO (try 0xC6 EM_CANUNDO)
	, "UInt"	, 0
	, "UInt"	, 0)
	DllCall("SendMessage" AW
		, Ptr		, hEdit
		, "UInt"	, 0x304		; WM_UNDO
		, "UInt"	, 0
		, "UInt"	, 0)
	DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
redo:
;================================================================
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0x454		; 0x455 EM_CANREDO (0x454 EM_REDO)
	, "UInt"	, 0
	, "UInt"	, 0)
DllCall("SetFocus", Ptr, hEdit)
return
;================================================================
clr:
;================================================================
StringTrimLeft, j, A_GuiControl, 1
GuiControl,, %j%,
goto chkhint
return
;================================================================
hide:
;================================================================
Gui, 1:Hide
Gui, 3:Hide
Gui, 4:Hide
Gui, 5:Hide
GuiControl, 1:, smout,
GuiControl, 1:, smwin,
Menu, Tray, Rename, Hide window, Show window
Menu, Tray, Add, Show window, action1
Hotkey, %hk1%, action1
return
;================================================================
send:
sendA:
;================================================================
StringRight, sendType, A_ThisLabel, 1
Gui, 1:Submit, NoHide
Clipboard := formatted
if ahide
	gosub hide
if !isWine			; Required for Linux/WINE to avoid hanging because window operations don't work
	{
	Sleep, 200
	WinActivate, %awin%
	Sleep, 200
	WinWaitActive, %awin%,, 5	; Don't wait indefinitely, it may hang if target window's been closed
	if ErrorLevel
		return	; Don't send the string to some random window
	Sleep, 600
	if sendType=A
		SendPlay, {Raw}%formatted%	; Has issues with non-English characters in Unicode version !!!
	else SendPlay ^v
	}
return
;================================================================
select:
;================================================================
if (A_GuiControl = "aclr" && A_GuiEvent != "Normal")
	return
optionK := GetKeyState("Ctrl", "P") | GetKeyState("LCtrl", "P") | GetKeyState("RCtrl", "P")
if A_GuiControl in smileW,smileY,smileM
	goto %A_GuiControl%
gosub getstr
strg := SubStr(buf, p1+1, j)
gosub %cLabel%
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xC2		; EM_REPLACESEL
	, "UInt"	, 0
	, "Str"	, strg)
VarSetCapacity(p1, 0)
VarSetCapacity(p2, 0)
DllCall("SetFocus", Ptr, hEdit)
return

getstr:
VarSetCapacity(p1, 4, 0)
VarSetCapacity(p2, 4, 0)
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xB0		; EM_GETSEL
	, Ptr		, &p1
	, Ptr		, &p2)
p1 := NumGet(p1, 0, "UInt")
p2 := NumGet(p2, 0, "UInt")
i := DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xE		; WM_GETTEXTLENGTH
	, "UInt"	, 0
	, "UInt"	, 0)
if (p1=p2) 
	{
	j := i
	p1=0
	DllCall("SendMessage" AW
		, Ptr		, hEdit
		, "UInt"	, 0xB1	; EM_SETSEL
		, Ptr		, p1
		, "UInt"	, -1)
	}
else j := p2 - p1
VarSetCapacity(buf, (i+1)*A_CharSize, 0)
DllCall("SendMessage" AW
	, Ptr		, hEdit
	, "UInt"	, 0xD		; WM_GETTEXT
	, "UInt"	, (i+1)*A_CharSize
	, Ptr		, &buf)
VarSetCapacity(buf, -1)
return
;================================================================
default:
;================================================================
Loop, %hkall%
	Hotkey, % hk%A_Index%, Off
; this needs completed with all hotkeys loop
Loop, %hkall%
	{
	a := A_Index
	Loop, 8
		GuiControl, 2:, % "Button" A_Index+10*(a-1), 0
	}
Loop, 12
	GuiControl, 2:, Button%A_Index%, 0
GuiControl, 2:, t1, 1
gosub th1
GuiControl, 2:, Button3, 1
GuiControl, 2:, Button7, 1
if !aot
	gosub aot
GuiControl, 2:, ahide, 1
GuiControl, 2:, ahsm, 1
GuiControl, 2:, k1, T
Gui, 2:Submit, NoHide
Loop, %hkall%
	Hotkey, % hk%A_Index%, action%A_Index%
modif=1
;goto settingsOK
return
;================================================================
hkoff:
;================================================================
Hotkey, %hk1%, Off
Menu, Tray, Rename, Disable hotkey, Enable hotkey
Menu, Tray, Add, Enable hotkey, hkon
return
;================================================================
hkon:
;================================================================
Hotkey, %hk1%, On
Menu, Tray, Rename, Enable hotkey, Disable hotkey
Menu, Tray, Add, Disable hotkey, hkoff
return
;================================================================
th1:
;================================================================
a=1
Gui, 2:Submit, NoHide
Hotkey, % hk%a%, % t%a% ? "on" : "off"
GuiControl, % "2:" (t%a% ? "+cBlue" : "+cRed"), t%a%
GuiControl, 2:MoveDraw, t%a%
Loop, 8
	GuiControl, % "2:" (t%a% ? "Enable" : "Disable"), % "Button" A_Index+8*(a-1)
GuiControl, % "2:" (t%a% ? "Enable" : "Disable"), Edit%a%
return
;================================================================
th2:
;================================================================
a=2
Gui, 2:Submit, NoHide
Hotkey, % hk%a%, % t%a% ? "on" : "off"
GuiControl, % "2:" (t%a% ? "+cBlue" : "+cRed"), t%a%
GuiControl, 2:MoveDraw, t%a%
Loop, 8
	GuiControl, % "2:" (t%a% ? "Enable" : "Disable"), % "Button" A_Index+8*(a-1)
GuiControl, % "2:" (t%a% ? "Enable" : "Disable"), Edit%a%
return
;================================================================
aot:
;================================================================
aot := !aot
Gui, % "1:" (aot ? "+" : "-") "AlwaysOnTop"
Menu, Tray, ToggleCheck, AlwaysOnTop
GuiControl,, aot, %aot%
modif=1
return
;================================================================
arun:
;================================================================
Gui, 2:Submit, NoHide
if arun
	FileCreateShortcut, %A_ScriptFullPath%, %A_Startup%\%appname%.lnk
else IfExist, %A_Startup%\%appname%.lnk
	FileDelete, %A_Startup%\%appname%.lnk
modif=1
return
;================================================================
ahide:
;================================================================
Gui, 2:Submit, NoHide
modif=1
return
;================================================================
ahsm:
;================================================================
Gui, 2:Submit, NoHide
GuiControl, 2:, ahsm, % ahsm ? "L" : "J"
modif=1
return
;================================================================
settings:
;================================================================
Loop, %hkall%
	Hotkey, % hk%A_Index%, Off
gosub initialize
Gui, 2:Show
return
;================================================================
initialize:
;================================================================
s := w9x ? "x" : ""
Loop, %hkall%
	{
	idx := A_Index
	IniRead, dhk%A_Index%, %inifile%, Hotkeys, %A_Index%, % defHk%A_Index%%s%
	StringReplace, hk%A_Index%, dhk%A_Index%, SHIFT+, +
	StringReplace, hk%A_Index%, hk%A_Index%, ALT+, !
	StringReplace, hk%A_Index%, hk%A_Index%, CTRL+, ^
	StringReplace, hk%A_Index%, hk%A_Index%, WIN+, #
	StringReplace, hk%A_Index%, hk%A_Index%, L-, <, All
	StringReplace, hk%A_Index%, hk%A_Index%, R-, >, All
	StringReplace, hk%A_Index%, hk%A_Index%, L., <, All
	StringReplace, hk%A_Index%, hk%A_Index%, R., >, All
	Loop, Parse, keys, CSV
		{
		i := A_LoopField
		IfInString, hk%idx%, %i%
			GuiControl, 2:, % "Button" 8*(idx-1)+(w9x ? 2*A_Index-1 : A_Index), 1
		}
	i := hk%idx%
	Loop, % StrLen(i)
	if i is not alnum
		StringTrimLeft, i, i, 1
	; send actual key to Edit control
	GuiControl, 2:, Edit%A_Index%, %i%
	StringLower, hk%idx%, hk%idx%
	}
return
;================================================================
settingsOK:
;================================================================
Gui, 2:Submit, NoHide
Loop, %hkall%	; to be replaced with number of hotkeys
	{
	idx := A_Index
	hk%idx%=
	Loop, Parse, keys, CSV
		{
		GuiControlGet, i, 2:, % "Button" 8*(idx-1)+(w9x ? 2*A_Index-1 : A_Index)
		if i
			hk%idx% .= A_LoopField
		}
	GuiControlGet, i, 2:, Edit%idx%
	i = %i%
	hk%idx% .= i
	StringReplace, dhk%A_Index%, hk%A_Index%, +, SHIFT+
	StringReplace, dhk%A_Index%, dhk%A_Index%, !, ALT+
	StringReplace, dhk%A_Index%, dhk%A_Index%, ^, CTRL+
	StringReplace, dhk%A_Index%, dhk%A_Index%, #, WIN+
	StringReplace, dhk%A_Index%, dhk%A_Index%, <, L., All
	StringReplace, dhk%A_Index%, dhk%A_Index%, >, R., All
	if t%idx%
		Hotkey, % hk%idx%, action%idx%
	IniWrite, % dhk%A_Index%, %inifile%, Hotkeys, %A_Index%
	}
;================================================================
2GuiClose:
2GuiEscape:
settingsHide:
;================================================================
Loop, %hkall%
	Hotkey, % hk%A_Index%, On
Gui, 2:Hide
return
;################################################################
;================================================================
bold:	; <strong> <b>
TB1C1:
;================================================================
strg := optionK ? "<b>" strg "</b>" : "<strong>" strg "</strong>"
return
;================================================================
ital:		; <em> <i>
TB1C2:
;================================================================
strg := optionK ? "<i>" strg "</i>" : "<em>" strg "</em>"
return
;================================================================
ulin:		; <u>
TB1C3:
;================================================================
strg := optionK ? "<u>" strg "</u>" : "<span style=""text-decoration: underline;"">" strg "</span>"
return
;================================================================
strk:		; <del datetime="2011-12-22T15:31:41+00:00"> <strike>
TB1C4:
;================================================================
FormatTime, ctime,, yyyy-MM-ddTHH:mm:ss+00:00
strg := optionK ? "<strike>" strg "</strike>" : "<del datetime=""" ctime """>" strg "</del>"
return
;================================================================
inst:		; <ins datetime="2011-12-22T15:31:41+00:00">
TB2C10:
;================================================================
FormatTime, ctime,, yyyy-MM-ddTHH:mm:ss+00:00
strg := "<ins datetime=""" ctime """>" strg "</ins>"
return
;################################################################
;================================================================
link:		; <a rel="nofollow" href="http://test.com" title="my test" target="_blank">text</a>
TB1C6:
;================================================================
Gui, 1:Submit, NoHide
if !url
	{
	MsgBox, 0x43010, %appname% %version%, Please fill in the URL field.
	return
	}
urltxt := !urltxt ? "link" : urltxt
strg := !strg ? urltxt : strg
k := optionK ? "nofollow" : "dofollow"
strg := "<a rel=""" k """ href=""" url """ title=""" urltxt """ target=""_blank"">" strg "</a>"
return
;================================================================
img:		; <img src="angry.gif" alt="Angry face" title="angry" border="0|1"
TB2C1:	; align="top|bottom|left|right|middle" width="??" height="??" /> 
;================================================================
Gui, 1:Submit, NoHide
if !strg
	{
	MsgBox, 0x43010, %appname% %version%, Please copy the image URL to the bottom field.
	return
	}
urltxt := !urltxt ? "link" : urltxt
urltip := !urltxt ? "" : urltxt	; may be assigned its own field at a later time
url := url ? url : strg
i := optionK ? !bord : bord	; the CTRL key inverts the current border option
strg := "<a href=""" url """ target=""_blank""><img src=""" strg """ alt=""" urltxt """ title=""" urltip
	. """ border=""" i """ /></a>"
return
;================================================================
quot:	; <blockquote cite="#commentbody-305"><strong>Drugwash</strong>
TB1C7:	; said <a href="#comment-305">here</a> :
;================================================================
Gui, 1:Submit, NoHide
j := optionK ? " a spus" : "said", k := optionK ? "aici" : "here"

if (!url && !urltxt)
	i := "<blockquote>"
else if !urltxt
	{
	MsgBox, 0x43010, %appname% %version%,
		(LTrim
		Please fill in the [alternative text] field above
		with the quoted comment's #.
		)
	return
	}
else
	{
	url := url ? url : "<i>" (optionK ? "Cineva" : "Somebody") "</i>"
	i := "<blockquote cite=""#commentbody-" urltxt """><strong>" url "</strong> " j
		. " <a href=""#comment-" urltxt """><strong>" k "</strong></a> `:`n"
	}
strg := i . strg "</blockquote>"
return
;================================================================
code:	; <code>
TB1C8:
;================================================================
strg := "<code>" strg "</code>"
return
;================================================================
more:	; <!--more-->
TB2C12:
;================================================================
strg .= "<!--more-->"
return
;================================================================
horz:	; <hr align="center" noshade="noshade" size="5" width="50%" />
TB2C13:
;================================================================
if urltxt between 1 and 10
	strg .= "<hr size=""" urltxt """ />"
else strg .= "<hr />"
return
;################################################################
;================================================================
indt:		; <p style="padding-left: 30px;">
TB2C2:
;================================================================
strg := "<p style=""padding-left: 30px;"">" strg "</p>"
return
;================================================================
cntr:		; <p style="text-align: center;">
TB2C3:
;================================================================
;strg := "<p style=""text-align: center;"">" strg "</p>"
Menu, aligntext, Show
return
;================================================================
align:
a := A_ThisMenuItemPos = "1" ? "left"
	: A_ThisMenuItemPos = "3" ? "right" : "center"
strg := "<p style=""text-align: " a ";"">" strg "</p>"
return
;================================================================
list:		; <ul><li> </li></ul>	<ol><li> </li></ol>
TB2C11:
;================================================================
i := optionK ? "ol" : "ul"
j=
k := i="ol" ? "1" : "disc"
Loop, Parse, strg, `n, `r
	if A_LoopField
		j .= "<li>" A_loopField "</li>`r`n"
strg := "<" i " type=""" k """>`r`n" j "</" i ">"
return
;================================================================
abbr:	; <abbr title="">
TB1C9:
;================================================================
if !urltxt
	{
	MsgBox, 0x43010, %appname% %version%,
		(LTrim
		Please fill in the [alternate text] field above
		with the full version of the abbreviated word.
		)
	return
	}
strg := "<abbr title=""" urltxt """>" strg "</abbr>"
return
;================================================================
acro:	;<acronym title="">
TB1C10:
;================================================================
if !urltxt
	{
	MsgBox, 0x43010, %appname% %version%,
		(LTrim
		Please fill in the [alternate text] field above
		with the full version of the acronym.
		)
	return
	}
strg := "<acronym title=""" urltxt """>" strg "</acronym>"
return
;################################################################
;================================================================
colr:		; <span style="color: #ff9900;">
TB2C4:
;================================================================
color := ChgColor(color)
if ErrorLevel
	return
; build new icons and replace in imagelists
IL_ReplaceColor(hTBIL8, 5, SwColor("0x" color), "BW")
IL_ReplaceColor(hTBIL9, 5, SwColor("0x" color), "BW")
; we need a method to refresh the toolbar display after changing color !
WinHide, ahk_id %hTB2%
WinShow, ahk_id %hTB2%
modif=1		; change it so it will save settings on exit
aclr:
TB2C5:
i := SubStr("000000" color, -5)
strg := "<span style=""color: #" i ";"">" strg "</span>"
return
;================================================================
szmenu:
;================================================================
gosub select
return
;================================================================
size:
TB2C6:
;================================================================
Menu, txtsize, Show
return
;================================================================
size1:
;================================================================
StringLower, mi, A_ThisMenuItem
strg := "<span style=""font-size: " mi ";"">" strg "</span>"
return
;================================================================
hdng:
;================================================================
TB2C14:
Menu, hdsize, Show
return
;================================================================
size2:
;================================================================
mi := A_ThisMenuItemPos
strg := "<h" mi ">" strg "</h" mi ">"
return
;################################################################
;================================================================
uppr:
TB1C5:
;================================================================
DllCall("CharUpper" AW, "Str", strg)
strg := strg
return
;================================================================
lowr:
TB1C11:
;================================================================
DllCall("CharLower" AW, "Str", strg)
strg := strg
return
;################################################################
;================================================================
supr:
TB2C7:
;================================================================
strg := "<sup>" strg "</sup>"
return
;================================================================
footnote:
TB2C16:
;================================================================
fn=1
gosub getstr
strg := SubStr(buf, p1+1, j)
GuiControl,, url, %strg%
;GuiControl, Hide, Curltxt
GuiControl, Hide, urltxt
GuiControl, Hide, hint2	; doesn't work correctly!
GuiControl, Show, notelist
; must be finished !
return
;################################################################
;================================================================
3GuiContextMenu:
4GuiContextMenu:
5GuiContextMenu:
Gui, %A_Gui%:Hide
GuiControl, 1:, smout,
GuiControl, 1:, smwin,
return
;================================================================
about:
;================================================================
IfNotExist, %appname%_readme.txt
	FileInstall, AddTags_readme.txt, %appname%_readme.txt
MsgBox, 0x43040, About %appname% %version%, 
(
(C) Drugwash, %releaseD%
~ most toolbar icons from the Silk package ~
~ by Mark James at famfamfam.com ~

~ for my blogger friends at wordpress.com ~

%releaseV%
please report any issues by e-mail at:
	%authormail%

 Use %dhk1% to show/hide.
 CTRL+button click offers alternative options.
 L-ALT toggles keeping smiley window open.
 In Y!M smiley window, CTRL or SHIFT offer
  alternatives: Y!M code or simple image link.
 %dhk2% (or 'Send 2' button) allows
   pasting text to copy-protected pages.

For details, please read the readme text file.
)
return
;================================================================
readme:
;================================================================
FileInstall, AddTags_readme.txt, %A_Temp%\%appname%\readme.txt, 1
Run, open %A_Temp%\%appname%\readme.txt,, UseErrorLevel
return
;================================================================
;		FUNCTIONS
;================================================================
GetSetFont(hSrc, hDest)
;================================================================
{
Global Ptr, AW
if hF := DllCall("SendMessage" AW
		, Ptr, hSrc
		, "UInt", 0x31		; WM_GETFONT
		, "UInt", 0
		, "UInt", 0
		, Ptr)
DllCall("SendMessage" AW
		, Ptr, hDest
		, "UInt", 0x30		; WM_SETFONT
		, Ptr, hF
		, "UInt", 0)
return hF
}
;================================================================
comm(wP, lP)					; WM_COMMAND HOOK
;================================================================
{
Global
Local hi
; Careful, 'hi' must be decimal, not hex !!!
hi := wP & 0xFFFF
hi=%hi%
if (lP=hTB0)
	{
	cLabel = TB0C%hi%
	SetTimer, select, -1
	}
else if (lP=hTB1)
	{
	cLabel = TB1C%hi%
	SetTimer, select, -1
	}
else if (lP=hTB2)
	{
	cLabel = TB2C%hi%
	SetTimer, select, -1
	}
if (((wP>>16) & 0xFFFF) != 0x200)
	return
if (lP != hE1 && lP != hE2)
	SetTimer, chkhint, -1

;EN_CHANGE=0x300 EN_SETFOCUS=0x100 EN_KILLFOCUS=0x200
}
;================================================================
btn1(wP, lP, msg, hWnd)		; WM_MOUSEMOVE HOOK
;================================================================
{
Global
Critical
optionK := GetKeyState("Ctrl", "P") | GetKeyState("LCtrl", "P") | GetKeyState("RCtrl", "P")
shiftK := GetKeyState("Shift", "P") | GetKeyState("LShift", "P") | GetKeyState("RShift", "P")
Loop, %tt0%
	if (hWnd = hBt%A_Index%)
		{
		GuiControl, 1:, smout, % optionK ? tta%A_Index% : tt%A_Index%
		break
		}
;if (hWnd=hMain OR hWnd=hEdit)		; I don't like how this behaves, stealing focus
;	DllCall("SetFocus", Ptr, hEdit)		; from legit controls/windows
if (hWnd=hSmile)
	{
	DllCall("SetFocus", Ptr, hSmile)
	MouseGetPos, x, y
	i := 1 + (x-1)//25 + 10*((y-1)//23)
	GuiControl, 3:, smCode, % smT%i% ? smT%i% : "< drag to move >"
	}
if (hWnd=hYSmile)
	{
	DllCall("SetFocus", Ptr, hYSmile)
	MouseGetPos, x, y
	i := 1 + (x-1)//40 + 10*((y-1)//20)
	GuiControl, 4:, ysmCode, % ysmT%i% ? ysmT%i% : "< drag to move >"
	GuiControl, 1:, smout, % optionK ? "Messenger code"
		: shiftK ? "Simple image link" : "HTML-formatted image code [Ctrl|Shift options]"
	}
if (hWnd=hMLSmile)
	{
	DllCall("SetFocus", Ptr, hMLSmile)
	MouseGetPos, x, y
	i := 1 + (x-1)//32 + 6*((y-1)//32)
	GuiControl, 5:, mlsmCode, % mlsmT%i% ? mlsmT%i% : "< drag to move >"
	}
}
;================================================================
btn2(wP, lP, msg, hWnd)
;================================================================
{
Global
GuiControl, 1:, smout,
}
;================================================================
key(wP, lP, msg, hwnd)
;================================================================
{
Global
ofi := A_FormatInteger
SetFormat, Integer, H
hwnd+=0
vk:=wP
if (hwnd = hh1)
	{
	sc:=lP >> 16
	if (vk=0x7b && sc=0x858)
		Send {BS}{BS}{BS}{F}{1}{2}
	else if (vk=0x7a && sc=0x857)
		Send {BS}{BS}{BS}{F}{1}{1}
	else if (vk=0x79) ;&& sc=0x856)	; for some reason, F10 doesn't send a message
		Send {BS}{BS}{BS}{F}{1}{0}
	else if (vk=0x78 && sc=0x843)
		Send {BS}{BS}{BS}{F}{9}{Space}
	else if (vk=0x77 && sc=0x842)
		Send {BS}{BS}{BS}{F}{8}{Space}
	else if (vk=0x76 && sc=0x841)
		Send {BS}{BS}{BS}{F}{7}{Space}
	else if (vk=0x75 && sc=0x840)
		Send {BS}{BS}{BS}{F}{6}{Space}
	else if (vk=0x74 && sc=0x83F)
		Send {BS}{BS}{BS}{F}{5}{Space}
	else if (vk=0x73 && sc=0x83E)
		Send {BS}{BS}{BS}{F}{4}{Space}
	else if (vk=0x72 && sc=0x83D)
		Send {BS}{BS}{BS}{F}{3}{Space}
	else if (vk=0x71 && sc=0x83C)
		Send {BS}{BS}{BS}{F}{2}{Space}
	else if (vk=0x70 && sc=0x83B)
		Send {BS}{BS}{BS}{F}{1}{Space}
	else Send {BS}{BS}{BS}{Space}{vk%vk%sc%sc%}{Space}
	}
SetFormat, Integer, %ofi%
if (hwnd != hYSmile)
	return
if vK not in 0x10,0x11,0x12	; SHIFT or CTRL pressed
	return
if (lP & 0x40000000)
	return
;k1 := GetKeyState("Alt", "P")
k2 := GetKeyState("Ctrl", "P") | GetKeyState("LCtrl", "P") | GetKeyState("RCtrl", "P")
k3 := GetKeyState("Shift", "P") | GetKeyState("LShift", "P") | GetKeyState("RShift", "P")
GuiControl, 1:, smout, % (k2 & k3) ? "Not supported" : (!k2 && k3) ? "Simple image link" : (k2 && !k3)
	? "Messenger code" : "HTML-formatted image code [Ctrl|Shift options]"
GuiControl, 1:, smwin, % k1 ? "Keep smiley window open      [Alt to toggle]" : ""
return 0
}
;================================================================
keyup(wP, lP, msg, hwnd)
;================================================================
{
Global
vK := wP
if (hwnd != hYSmile)
	return
if vK not in 16,17,18	; SHIFT or CTRL released
	return
k1 := vK=18 ? !k1 : k1
k2 := GetKeyState("Ctrl", "P") | GetKeyState("LCtrl", "P") | GetKeyState("RCtrl", "P")
k3 := GetKeyState("Shift", "P") | GetKeyState("LShift", "P") | GetKeyState("RShift", "P")
GuiControl, 1:, smout, % (k2 & k3) ? "Not supported" : (!k2 && k3) ? "Simple image link" : (k2 && !k3)
	? "Messenger code" : "HTML-formatted image code [Ctrl|Shift options]"
GuiControl, 1:, smwin, % k1 ? "Keep smiley window open      [Alt to toggle]" : ""
return 0
}
;================================================================
LoadImg(file, res="", t="B", sz="0")
;================================================================
{
Global Ptr, AW
Static hOwn, it="BIC"
if !hOwn
	hOwn := DllCall("GetModuleHandle" AW, Ptr, NULL)
if (file && res="")
	return DllCall("LoadImage" AW
			, Ptr		, 0
			, "Str"	, file
			, "UInt"	, 0
			, "Int"	, 0
			, "Int"	, 0
			, "UInt"	, 0x2010
			, Ptr)
if (res = "")
	return
if !file
	hInst := hOwn
else
	{
	if file is integer
		hinst := file
	else if !hInst := DllCall("GetModuleHandle" AW, "Str", file)
		hL := hInst := DllCall("LoadLibraryEx" AW, "Str", file, "UInt", 0, "UInt", 0x2, Ptr)
	; LOAD_LIBRARY_AS_DATAFILE. Don't use DONT_RESOLVE_DLL_REFERENCES !!!
	}
t := InStr(it, t)-1
if (t<0 OR hInst=0)
	return
flg := t>0 ? 0 : 0x2000
if res is not integer
	rt:="Str"
else rt:="UInt"
r := DllCall("LoadImage" AW
		, Ptr		, hInst
		, rt		, res
		, "UInt"	, t
		, "Int"	, sz
		, "Int"	, sz
		, "UInt"	, flg
		, Ptr)
if (hInst && hL)
	DllCall("FreeLibrary", Ptr, hInst)
return r
}
;================================================================
#include .\lib
#include func_ChgColor 1.9.4.ahk
#include func_GetHwnd.ahk
#include func_ImageList.ahk
#include func_Toolbar.ahk
#include functions.ahk
